<?php

namespace ProvisionningMyEC3\Tests\Product\Parapheur;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ProvisionningMyEC3\Entity\ProductUserSocle;
use ProvisionningMyEC3\Exception\ProductUserSocleNotFoundException;
use ProvisionningMyEC3\Http\Client\ParapheurClient;
use ProvisionningMyEC3\Http\Client\ParapheurClientException;
use ProvisionningMyEC3\Product\Parapheur\UserProvisioning;
use ProvisionningMyEC3\Repository\ProductUserSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;

class UserProvisioningTest extends TestCase
{

    private function getProductUserSocle(
        string $product,
        string $socleId,
        string $productId
    ): ProductUserSocle {
        $productOrganizationSocle = new ProductUserSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setUserSocleId($socleId);
        $productOrganizationSocle->setUserProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, '300005473', 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient->expects($this->once())
            ->method('createUser')
            ->willReturn([
                'id' => 123,
            ]);

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $parapheurClient);
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                0,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();


        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parapheurClient->expects($this->once())
            ->method('updateUser');

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $parapheurClient);
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'), 123, "")
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testDelete()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, '300005473', 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('getByUserProductId')
            ->with(UserProvisioning::PRODUCT_NAME, 123)
            ->willReturn($productUserSocle);
        $productUserSocleRepository->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parapheurClient->expects($this->once())
            ->method('deleteUser');

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $parapheurClient);
        $this->assertTrue($provisioningUser->delete(123));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetId()
    {
        $productUserSocle = $this->getProductUserSocle(UserProvisioning::PRODUCT_NAME, 7, 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this->getMockBuilder(ProductUserSocleRepository::class)->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository->expects($this->once())
            ->method('get')
            ->with(UserProvisioning::PRODUCT_NAME, 7)
            ->willReturn($productUserSocle);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning($productUserSocleRepository, $parapheurClient);
        $this->assertEquals(123, $provisioningUser->getId(7));
    }
}
