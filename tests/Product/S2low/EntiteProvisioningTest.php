<?php

namespace ProvisionningMyEC3\Tests\Product\S2low;

use PHPUnit\Framework\MockObject\MockObject;
use ProvisionningMyEC3\Entity\ProductOrganizationSocle;
use ProvisionningMyEC3\Exception\ProductOrganizationSocleNotFoundException;
use ProvisionningMyEC3\Http\Client\S2lowClient;
use ProvisionningMyEC3\Http\Client\S2lowClientException;
use ProvisionningMyEC3\Http\Model\S2lowAuthority;
use ProvisionningMyEC3\Product\S2low\EntiteProvisioning;
use PHPUnit\Framework\TestCase;
use ProvisionningMyEC3\Repository\ProductOrganizationSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;

class EntiteProvisioningTest extends TestCase
{

    private function getProductOrganizationSocle(
        string $product,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            EntiteProvisioning::PRODUCT_NAME,
            '100000005',
            123
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var S2lowClient|MockObject $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient->expects($this->once())
            ->method('createOrUpdateAuthority')
            ->willReturn([
                'id' => 123,
            ]);

        $provisioningUser = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $s2lowClient,
            [
                'authority_group_id' => 1,
                'department' => 1,
                'district' => 1,
                'authority_type_id' => 1,
                'service_name' => 'Root',
            ]
        );
        $this->assertTrue(
            $provisioningUser->add(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'))
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testUpdate()
    {
        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->getMock();

        /** @var S2lowClient|MockObject $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient->expects($this->once())
            ->method('getAuthority')
            ->with(123)
            ->willReturn(S2lowAuthority::hydrate([
                'id' => '123',
                'name' => 'Ville',
                'siren' => '000000000',
                'authority_group_id' => '1',
                'email' => 'email@example.org',
                'status' => '1',
                'authority_type_id' => '55',
                'address' => 'address',
                'postal_code' => '34000',
                'city' => 'Montpellier',
                'department' => '034',
                'district' => '3',
                'module' => [
                    1 => '1',
                    2 => '',
                    4 => '',
                ],
            ]));
        $s2lowClient->expects($this->once())
            ->method('createOrUpdateAuthority')
            ->willReturn([
                'id' => 123
            ]);

        $provisioningUser = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $s2lowClient,
            [
                'authority_group_id' => 1,
                'department' => 1,
                'district' => 1,
                'authority_type_id' => 1,
                'service_name' => 'Root',
            ]
        );
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'), 123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testDelete()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            EntiteProvisioning::PRODUCT_NAME,
            '100000005',
            123
        );
        $authority = S2lowAuthority::hydrate(
            [
                'id' => '123',
                'name' => 'Ville',
                'siren' => '000000000',
                'authority_group_id' => '1',
                'email' => 'email@example.org',
                'status' => '1',
                'authority_type_id' => '55',
                'address' => 'address',
                'postal_code' => '34000',
                'city' => 'Montpellier',
                'department' => '034',
                'district' => '3',
                'module' => [
                    1 => '1',
                    2 => '',
                    4 => '',
                ],
            ]
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->getMock();
        $productOrganizationSocleRepository->expects($this->once())
            ->method('getByOrganizationProductId')
            ->with(EntiteProvisioning::PRODUCT_NAME, 123)
            ->willReturn($productOrganizationSocle);
        $productOrganizationSocleRepository->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient->expects($this->once())
            ->method('getAuthority')
            ->with(123)
            ->willReturn($authority);

        $authority->status = 0;

        $s2lowClient->expects($this->once())
            ->method('createOrUpdateAuthority')
            ->with($authority)
            ->willReturn([]);

        $provisioningUser = new EntiteProvisioning($productOrganizationSocleRepository, $s2lowClient, [
            'authority_group_id' => 1,
            'department' => 1,
            'district' => 1,
            'authority_type_id' => 1,
        ]);
        $this->assertTrue($provisioningUser->delete(123));
    }

    /**
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function testGetId()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            EntiteProvisioning::PRODUCT_NAME,
            '100000005',
            123
        );
        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->getMock();
        $productOrganizationSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productOrganizationSocleRepository->expects($this->once())
            ->method('get')
            ->with(EntiteProvisioning::PRODUCT_NAME, 7)
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new EntiteProvisioning($productOrganizationSocleRepository, $s2lowClient, []);
        $this->assertEquals(123, $provisioningUser->getId(7));
    }
}
