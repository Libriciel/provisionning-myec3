FROM php:7.2-apache-stretch

RUN apt-get update && apt-get install -y \
    git \
    unzip \
   && rm -r /var/lib/apt/lists/*

# Installation de xdebug
RUN pecl install xdebug && \
    docker-php-ext-enable xdebug

# Ajout des extensions déjà présente
RUN docker-php-ext-enable opcache

# Installation de composer
RUN cd /tmp/ && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php --install-dir=/usr/local/bin && \
    mv /usr/local/bin/composer.phar /usr/local/bin/composer

# Répertoire de travail
WORKDIR /var/www/provisionning-myec3/

# Source de Pastell
COPY --chown=www-data:www-data ./ /var/www/provisionning-myec3/

# Module d'Apache
RUN a2enmod \
    rewrite \
    ssl

ENV PATH="${PATH}:/usr/local/lib/composer/vendor/bin"

EXPOSE 443 80

#Composer
RUN composer install
ENV PATH="${PATH}:/var/www/provisionning-myec3/vendor/bin/"

# Configuration d'apache
COPY ./ci-resources/apache2/provisionning-myec3.conf /etc/apache2/sites-available/provisionning-myec3.conf
RUN a2ensite provisionning-myec3.conf

VOLUME /etc/apache2/ssl/

# Entrypoint
COPY ./ci-resources/entrypoint.sh /usr/local/bin/
RUN chmod a+x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
CMD ["apache2-foreground"]

