<?php

namespace ProvisionningMyEC3\Http\Client;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

class ParapheurClient
{
    public const LOGIN_ENDPOINT = '/api/login';
    public const USER_ENDPOINT = '/parapheur/utilisateurs';

    /**
     * @var string
     */
    private $baseApi = '';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var RequestFactoryInterface
     */
    private $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    private $streamFactory;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $accessTicket;


    public function __construct(
        ClientInterface $httpClient = null,
        RequestFactoryInterface $requestFactory = null,
        StreamFactoryInterface $streamFactory = null
    ) {
        $this->httpClient = $httpClient ?? Psr18ClientDiscovery::find();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
    }

    public function setBaseApi(string $baseApi)
    {
        $this->baseApi = $baseApi;
    }

    public function setCredentials(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function getAccessTicket(): string
    {
        if (empty($this->accessTicket)) {
            $this->requestAccessTicket();
        }

        return $this->accessTicket;
    }


    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    private function requestAccessTicket()
    {
        $endpoint = $this->baseApi . self::LOGIN_ENDPOINT;

        $data = [
            'username' => $this->username,
            'password' => $this->password
        ];

        $request = $this->requestFactory
            ->createRequest('POST', $endpoint)
            ->withAddedHeader('Content-Type', 'application/json')
            ->withBody($this->streamFactory->createStream(json_encode($data)));
        $response = $this->httpClient->sendRequest($request);

        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        $content = json_decode($body, true);
        $this->accessTicket = $content['data']['ticket'];
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function createUser(
        string $username,
        string $firstname,
        string $lastname,
        string $email,
        string $password
    ): array {
        $data = [
            'username' => $username,
            'firstName' => $firstname,
            'lastName' => $lastname,
            'email' => $email,
            'password' => $password
        ];

        $request = $this->requestFactory
            ->createRequest('POST', $this->getUrl(self::USER_ENDPOINT))
            ->withAddedHeader('Content-Type', 'application/json')
            ->withBody($this->streamFactory->createStream(json_encode($data)));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        $decodedBody = json_decode($body, true);
        if (
            !$decodedBody['id'] ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $decodedBody['id'])
        ) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return $decodedBody;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function updateUser(string $id, string $firstname, string $lastname, string $email)
    {
        $data = [
            'firstName' => $firstname,
            'lastName' => $lastname,
            'email' => $email
        ];
        $request = $this->requestFactory
            ->createRequest('PUT', $this->getUrl(self::USER_ENDPOINT . "/$id"))
            ->withAddedHeader('Content-Type', 'application/json')
            ->withBody($this->streamFactory->createStream(json_encode($data)));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return json_decode($body, true);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function deleteUser(string $id)
    {
        $request = $this->requestFactory
            ->createRequest('DELETE', $this->getUrl(self::USER_ENDPOINT . "/$id"))
            ->withAddedHeader('Content-Type', 'application/json');

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return json_decode($body, true);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    private function getUrl(string $endpoint, array $queryParameters = []): string
    {
        return $this->baseApi . $endpoint . '?' .
            http_build_query(
                ['ticket' => $this->getAccessTicket()] + $queryParameters
            );
    }
}
