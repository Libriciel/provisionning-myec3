<?php

namespace ProvisionningMyEC3\Repository;

use ProvisionningMyEC3\Entity\ProductOrganizationSocle;
use ProvisionningMyEC3\Exception\ProductOrganizationSocleNotFoundException;

interface ProductOrganizationSocleRepository
{
    /**
     * @param string $productId
     * @param string $organizationSocleId
     * @return ProductOrganizationSocle
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function get(string $productId, string $organizationSocleId): ProductOrganizationSocle;

    public function getByOrganizationProductId(
        string $productId,
        string $organizationProductId
    ): ProductOrganizationSocle;

    public function exists(string $productId, string $organizationSocleId): bool;

    public function add(ProductOrganizationSocle $productOrganizationSocle): bool;

    public function delete(ProductOrganizationSocle $productOrganizationSocle): bool;
}
