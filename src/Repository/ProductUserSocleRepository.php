<?php

namespace ProvisionningMyEC3\Repository;

use ProvisionningMyEC3\Entity\ProductUserSocle;
use ProvisionningMyEC3\Exception\ProductUserSocleNotFoundException;

interface ProductUserSocleRepository
{
    /**
     * @param string $productId
     * @param string $userSocleId
     * @return ProductUserSocle
     * @throws ProductUserSocleNotFoundException
     */
    public function get(string $productId, string $userSocleId): ProductUserSocle;

    public function getByUserProductId(string $productId, string $userProductId): ProductUserSocle;

    public function exists(string $productId, string $userSocleId): bool;

    public function add(ProductUserSocle $productUserSocle): bool;

    public function delete(ProductUserSocle $productUserSocle): bool;
}
