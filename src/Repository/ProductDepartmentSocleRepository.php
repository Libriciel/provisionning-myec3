<?php

namespace ProvisionningMyEC3\Repository;

use ProvisionningMyEC3\Entity\ProductDepartmentSocle;

interface ProductDepartmentSocleRepository
{
    public function get(string $productId, string $departmentSocleId): ProductDepartmentSocle;

    public function getByDepartmentProductId(string $productId, string $departmentProductId): ProductDepartmentSocle;

    public function exists(string $productId, string $departmentSocleId): bool;

    public function add(ProductDepartmentSocle $productDepartmentSocle): bool;

    public function delete(ProductDepartmentSocle $productDepartmentSocle): bool;
}
