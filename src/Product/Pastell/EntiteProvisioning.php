<?php

namespace ProvisionningMyEC3\Product\Pastell;

use Libriciel\Myec3\StructureSocleInterface;
use Pastell\Api\EntitesRequester;
use Pastell\Hydrator\EntiteHydrator;
use ProvisionningMyEC3\Entity\ProductOrganizationSocle;
use ProvisionningMyEC3\Exception\ProductOrganizationSocleNotFoundException;
use ProvisionningMyEC3\Repository\ProductOrganizationSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{
    public const PRODUCT_NAME = 'pastell';

    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;

    /**
     * @var EntitesRequester
     */
    private $entitesRequester;

    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;


    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        EntitesRequester $entitesRequester,
        EntiteHydrator $entiteHydrator = null
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->entitesRequester = $entitesRequester;
        $this->entiteHydrator = $entiteHydrator ?? new EntiteHydrator();
    }

    /**
     * Add structure
     * @param SimpleXMLElement $xml
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $entite = $this->getEntiteFromXml($xml);

        $response = $this->entitesRequester->create($this->entiteHydrator->hydrate($entite));

        $productUserSocle = new ProductOrganizationSocle();
        $productUserSocle->setProductId(self::PRODUCT_NAME);
        $productUserSocle->setOrganizationSocleId($entite['socleId']);
        $productUserSocle->setOrganizationProductId($response->getId());
        return $this->productOrganizationSocleRepository->add($productUserSocle);
    }

    /**
     * update a structure
     * @param SimpleXMLElement $xml
     * @param string|int $structureId
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        $entite = $this->getEntiteFromXml($xml);
        $pastellEntite = $this->entitesRequester->show($structureId);
        $pastellEntite->denomination = $entite['denomination'];
        $pastellEntite->siren = $entite['siren'];
        $this->entitesRequester->update($pastellEntite);

        return true;
    }

    /**
     * @param string|int $structureId
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function delete($structureId): bool
    {
        $productOrganizationSocle = $this->productOrganizationSocleRepository->getByOrganizationProductId(
            self::PRODUCT_NAME,
            $structureId
        );

        $this->entitesRequester->remove($structureId);
        return $this->productOrganizationSocleRepository->delete($productOrganizationSocle);
    }

    /**
     * get structureId
     * @param string $externalId
     * @return string|int|null
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if ($this->productOrganizationSocleRepository->exists(self::PRODUCT_NAME, $externalId)) {
            $productOrganizationSocle = $this->productOrganizationSocleRepository->get(self::PRODUCT_NAME, $externalId);
            return $productOrganizationSocle->getOrganizationProductId();
        }
        return null;
    }

    /**
     * get role Id
     * @param string|int $roleName
     * @param string|int $structureId
     * @return string|int|null
     */
    public function getRoleId($roleName, $structureId)
    {
        return $roleName;
    }

    private function getEntiteFromXml(SimpleXMLElement $xml): array
    {
        return [
            'socleId' => (string)$xml->externalId,
            'denomination' => (string)$xml->label,
            'siren' => (string)$xml->siren,
            'type' => 'collectivite',
            'entite_mere' => '0'
        ];
    }
}
