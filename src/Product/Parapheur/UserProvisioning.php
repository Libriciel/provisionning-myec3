<?php

namespace ProvisionningMyEC3\Product\Parapheur;

use Exception;
use Libriciel\Myec3\UserSocleInterface;
use ProvisionningMyEC3\Entity\ProductUserSocle;
use ProvisionningMyEC3\Exception\ProductUserSocleNotFoundException;
use ProvisionningMyEC3\Http\Client\ParapheurClient;
use ProvisionningMyEC3\Http\Client\ParapheurClientException;
use ProvisionningMyEC3\Repository\ProductUserSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class UserProvisioning implements UserSocleInterface
{
    public const PRODUCT_NAME = 'iparapheur';

    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;

    /**
     * @var ParapheurClient
     */
    private $parapheurClient;


    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        ParapheurClient $parapheurClient
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->parapheurClient = $parapheurClient;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     * @throws Exception
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);

        $response = $this->parapheurClient->createUser(
            $user['login'],
            $user['prenom'],
            $user['nom'],
            $user['email'],
            base64_encode(random_bytes(20))
        );

        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId(self::PRODUCT_NAME);
        $productUserSocle->setUserSocleId($user['socleId']);
        $productUserSocle->setUserProductId($response['id']);
        return $this->productUserSocleRepository->add($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function update(SimpleXMLElement $xml, $userId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);
        $this->parapheurClient->updateUser($userId, $user['prenom'], $user['nom'], $user['email']);

        return true;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function delete($userId): bool
    {
        $productUserSocle = $this->productUserSocleRepository->getByUserProductId(self::PRODUCT_NAME, $userId);
        $this->parapheurClient->deleteUser($userId);

        return $this->productUserSocleRepository->delete($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ProductUserSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if (!$this->productUserSocleRepository->exists(self::PRODUCT_NAME, $externalId)) {
            return null;
        }
        return $this->productUserSocleRepository->get(self::PRODUCT_NAME, $externalId)->getUserProductId();
    }

    private function getUserFromXml(SimpleXMLElement $xml)
    {
        return [
            'socleId' => (string)$xml->externalId,
            'login' => (string)$xml->alfUserName,
            'prenom' => (string)$xml->user->firstname,
            'nom' => (string)$xml->user->lastname,
            'email' => (string)$xml->email,
        ];
    }
}
