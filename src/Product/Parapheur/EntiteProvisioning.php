<?php

namespace ProvisionningMyEC3\Product\Parapheur;

use Libriciel\Myec3\StructureSocleInterface;
use ProvisionningMyEC3\Http\Client\ParapheurClient;
use ProvisionningMyEC3\Repository\ProductOrganizationSocleRepository;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{
    public const PRODUCT_NAME = 'iparapheur';

    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;

    /**
     * @var ParapheurClient
     */
    private $parapheurClient;


    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        ParapheurClient $parapheurClient
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->parapheurClient = $parapheurClient;
    }

    /**
     * @inheritDoc
     */
    public function add(SimpleXMLElement $xml): bool
    {
        // TODO: Implement add() method.
    }

    /**
     * @inheritDoc
     */
    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        // TODO: Implement update() method.
    }

    /**
     * @inheritDoc
     */
    public function delete($structureId): bool
    {
        // TODO: Implement delete() method.
    }

    /**
     * @inheritDoc
     */
    public function getId(string $externalId)
    {
        return $externalId;
    }

    /**
     * @inheritDoc
     */
    public function getRoleId($roleName, $structureId)
    {
        return $roleName;
    }
}
