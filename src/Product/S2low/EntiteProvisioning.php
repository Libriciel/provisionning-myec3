<?php

namespace ProvisionningMyEC3\Product\S2low;

use Libriciel\Myec3\StructureSocleInterface;
use ProvisionningMyEC3\Entity\ProductOrganizationSocle;
use ProvisionningMyEC3\Exception\ProductOrganizationSocleNotFoundException;
use ProvisionningMyEC3\Http\Client\S2lowClient;
use ProvisionningMyEC3\Http\Client\S2lowClientException;
use ProvisionningMyEC3\Http\Model\S2lowAuthority;
use ProvisionningMyEC3\Repository\ProductOrganizationSocleRepository;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{
    public const PRODUCT_NAME = 's2low';

    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var S2lowClient
     */
    private $s2lowClient;
    /**
     * @var array
     */
    private $defaultAuthorityValues;

    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        S2lowClient $s2lowClient,
        array $defaultAuthorityValues
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->s2lowClient = $s2lowClient;
        $this->defaultAuthorityValues = $defaultAuthorityValues;
    }

    /**
     * @inheritDoc
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $authority = $this->getAuthorityFromXml($xml);
        $authority->authority_group_id = $this->defaultAuthorityValues['authority_group_id'];
        $authority->department = $this->defaultAuthorityValues['department'];
        $authority->district = $this->defaultAuthorityValues['district'];
        $authority->authority_type_id = $this->defaultAuthorityValues['authority_type_id'];
        $authority->setActModule();
        $authority->setHeliosModule();

        $response = $this->s2lowClient->createOrUpdateAuthority($authority);
        $this->s2lowClient->createAuthorityService($response['id'], $this->defaultAuthorityValues['service_name']);

        $productUserSocle = new ProductOrganizationSocle();
        $productUserSocle->setProductId(self::PRODUCT_NAME);
        $productUserSocle->setOrganizationSocleId((string)$xml->externalId);
        $productUserSocle->setOrganizationProductId($response['id']);
        return $this->productOrganizationSocleRepository->add($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        $authority = $this->s2lowClient->getAuthority($structureId);
        $authority->name = (string)$xml->label;
        $authority->email = (string)$xml->email;
        $authority->address = (string)$xml->address->postalAddress;
        $authority->postal_code = (string)$xml->address->postalCode;
        $authority->city = (string)$xml->address->city;

        $response = $this->s2lowClient->createOrUpdateAuthority($authority);
        $services = $this->s2lowClient->getAuthorityServices($response['id']);
        if (empty($services)) {
            $this->s2lowClient->createAuthorityService($response['id'], $this->defaultAuthorityValues['service_name']);
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function delete($structureId): bool
    {
        $productOrganizationSocle = $this->productOrganizationSocleRepository->getByOrganizationProductId(
            self::PRODUCT_NAME,
            $structureId
        );
        $authority = $this->s2lowClient->getAuthority($structureId);
        $authority->status = 0;
        $this->s2lowClient->createOrUpdateAuthority($authority);

        return $this->productOrganizationSocleRepository->delete($productOrganizationSocle);
    }

    /**
     * @inheritDoc
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if ($this->productOrganizationSocleRepository->exists(self::PRODUCT_NAME, $externalId)) {
            $productOrganizationSocle = $this->productOrganizationSocleRepository->get(self::PRODUCT_NAME, $externalId);
            return $productOrganizationSocle->getOrganizationProductId();
        }
        return null;
    }

    /**
     * @inheritDoc
     *
     * Returns the id of the organism so we don't have to heavily modify the Socle class
     */
    public function getRoleId($roleName, $structureId)
    {
        return $structureId;
    }

    private function getAuthorityFromXml(SimpleXMLElement $xml): S2lowAuthority
    {
        $authority = new S2lowAuthority();
        $authority->name = (string)$xml->label;
        $authority->siren = (string)$xml->siren;
        $authority->email = (string)$xml->email;
        $authority->status = 1;
        $authority->authority_type_id = (string)$xml->user->certificate;
        $authority->address = (string)$xml->address->postalAddress;
        $authority->postal_code = (string)$xml->address->postalCode;
        $authority->city = (string)$xml->address->city;

        return $authority;
    }
}
