# 1.4.3

- mise à jour des dépendances

# 1.4.2

## Correction

- Ajout de l'id_e d'entité de base lors de la création d'un utilisateur sur Pastell

# 1.0.0

## Ajout

- Création initiale