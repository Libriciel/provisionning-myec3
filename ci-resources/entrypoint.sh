#! /bin/bash
set -e

# Activation ou non de XDEBUG
if [ -z ${XDEBUG_ON} ]
then
    if [ -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ]
    then
        rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    fi
else
    docker-php-ext-enable xdebug
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

bash /var/www/provisionning-myec3/ci-resources/generate-key-pair.sh localhost

exec "$@"
